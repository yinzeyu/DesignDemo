package com.yin97.design_master;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private CommonTabLayout commonTabLayout;
    private String[] mTitles = {"首页", "美女","视频","关注"};
    private int[] mIconUnselectIds = {
            R.mipmap.ic_home_normal,R.mipmap.ic_girl_normal,R.mipmap.ic_video_normal,R.mipmap.ic_care_normal};
    private int[] mIconSelectIds = {
            R.mipmap.ic_home_selected,R.mipmap.ic_girl_selected, R.mipmap.ic_video_selected,R.mipmap.ic_care_selected};
    private int tabLayoutHeight;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private HomeFragment homeFragment;
    private VideoFragment videoFragment;
    private MyFragment myFragment;
    private  GrilFragment grilFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         commonTabLayout = (CommonTabLayout) findViewById(R.id.commonTabLayout);
        commonTabLayout.measure(0,0);
        ViewTreeObserver vto2 = commonTabLayout.getViewTreeObserver();
        vto2.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                commonTabLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                tabLayoutHeight=commonTabLayout.getMeasuredHeight();
            }
        });
        initTab();
        index(0);

    }

    /**
     * 初始化tab
     */
    private void initTab() {
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]));
        }
        commonTabLayout.setTabData(mTabEntities);
        //点击监听
        commonTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
               index(position);
            }
            @Override
            public void onTabReselect(int position) {
            }
        });
    }
    private  void  index(int position){
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        hideFragments(fragmentTransaction);
        //  mViewHolderTitle.mTitleTextView.setText(strings.get(position));
        switch (position){
            case 0:
                if (homeFragment==null){
                    homeFragment=new HomeFragment();
                    fragmentTransaction.add(R.id.base_activity_content,homeFragment);

                }else {
                    fragmentTransaction.show(homeFragment);
                }

                break;
            case 1:
                if (grilFragment==null){
                    grilFragment=new GrilFragment();
                    fragmentTransaction.add(R.id.base_activity_content,grilFragment);

                }else {
                    fragmentTransaction.show(grilFragment);
                }
                break;
            case 2:
                if (videoFragment==null){
                    videoFragment=new VideoFragment();
                    fragmentTransaction.add(R.id.base_activity_content,videoFragment);

                }else {
                    fragmentTransaction.show(videoFragment);
                }

                break;
            case 3:
                if (myFragment==null){
                    myFragment=new MyFragment();
                    fragmentTransaction.add(R.id.base_activity_content,myFragment);

                }else {
                    fragmentTransaction.show(myFragment);
                }

                break;


        }
        fragmentTransaction.commit();
    }
    /**
     * 隐藏fragment
     * @param transaction
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (homeFragment != null) {
            transaction.hide(homeFragment);
        }
        if (videoFragment != null) {
                transaction.hide(videoFragment);
        }
        if (myFragment != null) {
            transaction.hide(myFragment);
        }
        if (grilFragment!=null){
            transaction.hide(grilFragment);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 普通事件
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataSynEvent(DataSynEvent event) {
        startAnimation(event.isfend);

      //  mBottomSheetBehavior.setState(event.isfend ? BottomSheetBehavior.STATE_EXPANDED : BottomSheetBehavior.STATE_COLLAPSED);
    }

    /**
     * 菜单显示隐藏动画
     * @param showOrHide
     */
    private void startAnimation(boolean showOrHide){
        final ViewGroup.LayoutParams layoutParams = commonTabLayout.getLayoutParams();
        ValueAnimator valueAnimator;
        ObjectAnimator alpha;
        if(showOrHide==true){
            valueAnimator = ValueAnimator.ofInt(tabLayoutHeight, 0);
            alpha = ObjectAnimator.ofFloat(commonTabLayout, "alpha", 1, 0);
        }else{
            valueAnimator = ValueAnimator.ofInt(0, tabLayoutHeight);
            alpha = ObjectAnimator.ofFloat(commonTabLayout, "alpha", 0, 1);
        }
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                layoutParams.height= (int) valueAnimator.getAnimatedValue();
                commonTabLayout.setLayoutParams(layoutParams);
            }
        });
        AnimatorSet animatorSet=new AnimatorSet();
        animatorSet.setDuration(500);
        animatorSet.playTogether(valueAnimator,alpha);
        animatorSet.start();
    }
}
