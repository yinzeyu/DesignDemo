package com.yin97.design_master;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private View mView;
    private Toolbar id_toobar;
    private AppBarLayout mAppBarLayout;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private FloatingActionButton mFloatingActionButton;
    private String[] mTitles;
    private List<Fragment> mFragments;
    private MyViewPagerAdapter mViewPagerAdapter;
    FloatingActionButton id_floatingactionbutton;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.home_fragment, null);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
        initData();
        configViews();
    }
    private void initViews() {
        id_toobar= (Toolbar) mView.findViewById(R.id.id_toobar);
        mAppBarLayout = (AppBarLayout) mView.findViewById(R.id.id_appbarlayout);
        mTabLayout = (TabLayout) mView.findViewById(R.id.id_tablayout);
        mViewPager = (ViewPager) mView.findViewById(R.id.id_viewpager);
        mFloatingActionButton = (FloatingActionButton) mView.findViewById(R.id.id_floatingactionbutton);
        id_floatingactionbutton= (FloatingActionButton) mView.findViewById(R.id.id_floatingactionbutton);
    }
    private void initData() {
        mTitles = getResources().getStringArray(R.array.tab_titles);
        mFragments = new ArrayList<>();
        for (int i = 0; i < mTitles.length; i++) {
            Bundle mBundle = new Bundle();
            mBundle.putInt("flag", i);
            MFragment mFragment = new MFragment();
            mFragment.setArguments(mBundle);
            mFragments.add(i, mFragment);
        }
    }
    private void configViews() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(id_toobar);
//        ActionBarDrawerToggle mActionBarDrawerToggle =
//                new ActionBarDrawerToggle(getActivity(), drawerLayout, id_toobar, R.string.open, R.string.close);
//        mActionBarDrawerToggle.syncState();
//        drawerLayout.setDrawerListener(mActionBarDrawerToggle);
//        //给NavigationView填充顶部区域，也可在xml中使用app:headerLayout="@layout/header_nav"来设置
//        mNavigationView.inflateHeaderView(R.layout.header_nav);
//        //给NavigationView填充Menu菜单，也可在xml中使用app:menu="@menu/menu_nav"来设置
//        mNavigationView.inflateMenu(R.menu.menu_nav);

        // 自己写的方法，设置NavigationView中menu的item被选中后要执行的操作
       // onNavgationViewMenuItemSelected(mNavigationView);

        // 初始化ViewPager的适配器，并设置给它
        mViewPagerAdapter = new MyViewPagerAdapter(getChildFragmentManager(), mTitles, mFragments);
        mViewPager.setAdapter(mViewPagerAdapter);
        // 设置ViewPager最大缓存的页面个数
        // 给ViewPager添加页面动态监听器（为了让Toolbar中的Title可以变化相应的Tab的标题）
        mViewPager.addOnPageChangeListener(this);
        // 将TabLayout和ViewPager进行关联，让两者联动起来
        mTabLayout.setupWithViewPager(mViewPager);
        // 设置Tablayout的Tab显示ViewPager的适配器中的getPageTitle函数获取到的标题
        mTabLayout.setTabsFromPagerAdapter(mViewPagerAdapter);

        // 设置FloatingActionButton的点击事件
        mFloatingActionButton.setOnClickListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {

    }
}
